﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace MultiplesOf3And5
{
    public static class MultiplesOf3Or5
    {
        public static int Run(int input)
        {

            var MultiplesOf3or5 = new List<int>();

            for (var i = 1; i < input; i++)
            {
                if (i%3 == 0 || i%5 == 0)
                {
                    MultiplesOf3or5.Add(i);
                }
            }

            return MultiplesOf3or5.Sum();
        }
    }

    public class Test
    {
        [Test]
        [TestCase(10, Result=23)]
        [TestCase(20, Result=78)]
        [TestCase(1000, Result = 233168)]
        public int WhenGivenInput_ReturnExpected(int input)
        {
            return MultiplesOf3Or5.Run(input);
        }
    }
}
